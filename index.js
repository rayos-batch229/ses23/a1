// console.log("Hello!")

let trainer = {};
	
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemons = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
	kanto: ["Brock", "Misty"],
	hoenn:["May", "Max"]
}


trainer.talk = function(){
	console.log("Pikachu! I choose you!");
}
	
	console.log(trainer.name);
	console.log(trainer)

function pokemon(name,level){

	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;
	this.tackle = function(attackenemy){
		console.log(this.name + " tackled " + attackenemy.name + ".");
	}
	this.faint = function(){
		console.log(this.name + " fainted ");
	}
}
console.log("Result of dot notation");
console.log(trainer.name);

console.log("Result of square bracket notation");
console.log(trainer["pokemons"]);

console.log("Result of talk method");
trainer.talk();



let pikachu = new pokemon("Pikachu", 12);
	console.log(pikachu);

let geodude = new pokemon("Geodude", 8);
	console.log(geodude);

let mewtwo = new pokemon("Mewtwo", 100);
	console.log(mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);
mewtwo.faint();